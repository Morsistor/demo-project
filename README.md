# demo project

This project serves the purpose of demonstrating the basic workflow using GitLab.
It is a very rudimentary Flask server only able to show one site.


## Technologies used

Flask, Python, HTML, JavaScript


## How to run
1. Install Flask. See: http://flask.pocoo.org/docs/0.12/installation/#installation
2. Clone this repository and go to the project directory.
3. Enter `$ export FLASK_APP=hello.py`, then `$ flask run` into console.
4. Click on the link that should appear in the console once Flask is running.
5. This should now open a browser showing the website created by this demo project.
6. To shutdown the server press `Ctrl` + `c` in the console, in which you started it.

To activate debug mode simply enter `$ export FLASK_DEBUG=1` before running FLASK.


## Contributors

*Listed alphabetically by last name.*

- Alex Dmitriev
- Frederick Gnodtke
- Maurice Grages
